import java.util.Scanner;

public class Aula3 {

public static void main(String[] args) {
 int a = 0, b = 0;
 double divisao = 0;
 Scanner teclado = new Scanner(System.in);
 
 System.out.println("Digite um valor inteiro: ");
 a = teclado.nextInt();
 
 do {
  System.out.println("Digite outro valor inteiro: ");
  b = teclado.nextInt();
 } while(b == 0);
 
 divisao = a/b;
 System.out.println("A divisao do primeiro valor pelo segundo eh: " + divisao);
}

}